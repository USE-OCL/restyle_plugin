#!/bin/sh
##
## build.sh for build in /home/vincent/USE/restyle_plugin
##
## Made by
## <vincent.davoust@gmail.com>
##
## Started on  Mon May  7 14:03:53 2018
## Last update Fri May 25 10:20:40 2018 
##

usePath=/home/vincent/USE/use-5.0.1/
plugin=restyle

if [[ $# -eq 1 ]]
then
    usePath=$1
else
    echo "Usage : %> ./build.sh [PATH_TO_USE_INSTALL]"
    echo ""
    echo "Ex : %> ./build.use /opt/use-5.0.1/"
fi

cd org/tzi/use/$plugin
javac -cp ${usePath}/lib/use.jar *.java
for d in ./*/ ; do
    cd $d ;
    javac -cp ${usePath}/lib/use.jar:../../../../../ *.java ;
    cd .. ;
done;

cd ../../../
pwd
cd org/tzi/use/gui/plugins/${plugin}
javac -cp ${usePath}/lib/use.jar:../../../../../../ *.java
cd ../../../../../../
jar cfm ${plugin}_plugin.jar Manifest.txt resources org useplugin.xml
cp ./${plugin}_plugin.jar ${usePath}/lib/plugins/
