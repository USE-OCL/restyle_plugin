#!/bin/sh
##
## clean.sh for clean in /home/vincent/USE/restyle_plugin
##
## Made by
## <vincent.davoust@gmail.com>
##
## Started on  Mon May 14 15:35:14 2018
## Last update Fri May 25 10:10:06 2018 
##



rm org/tzi/use/gui/plugins/restyle/*.class

for d in ./org/tzi/use/restyle/*/ ; do
    rm $d*.class
done;
