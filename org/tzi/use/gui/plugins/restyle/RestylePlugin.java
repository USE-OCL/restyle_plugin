package org.tzi.use.gui.plugins.restyle;

import org.tzi.use.runtime.IPlugin;
import org.tzi.use.runtime.IPluginRuntime;
import org.tzi.use.runtime.impl.Plugin;

public class RestylePlugin extends Plugin implements IPlugin {

    final protected String PLUGIN_ID = "RestylePlugin";

    public String getName() {
	return this.PLUGIN_ID;
    }

    public void run(IPluginRuntime pluginRuntime) throws Exception {

	// change graphic properties here !

    }

}
